package com.rfe.news.test;

import com.rfe.news.entity.Author;
import com.rfe.news.test.utils.EntityIdentifierComparator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AuthorDAOTest extends AbstractDAOTest {

    @After
    public void tearDown() throws Exception {
        removeEntityStack.deleteEntities();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testCreate() throws Exception {
        Author createdAuthor = authorDAO.create(buildAuthor());
        removeEntityStack.add(createdAuthor);

        Author authorRetrieved = authorDAO.read(createdAuthor.getId());

        Assert.assertEquals(createdAuthor, authorRetrieved);
    }

    @Test
    public void testDelete() throws Exception {
        Author createdAuthor = authorDAO.create(buildAuthor());
        removeEntityStack.add(createdAuthor);

        Integer authorId = createdAuthor.getId();

        authorDAO.delete(authorId);
        Assert.assertNull(authorDAO.read(authorId));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testUpdate() throws Exception {
        Author expected = authorDAO.create(buildAuthor());
        removeEntityStack.add(expected);

        expected.setAuthorName("Some_New_Author_Name");
        authorDAO.update(expected);

        Author updatedOne = authorDAO.read(expected.getId());

        Assert.assertEquals(expected, updatedOne);
    }

    @Test
    public void testWhereIdIn() throws Exception {
        List<Author> createdAuthors = new ArrayList<>();
        List<Integer> authorsIds = new ArrayList<>();
        for (int index = 0; index < 10; index++) {
            Author author = authorDAO.create(buildAuthor());
            createdAuthors.add(author);
            authorsIds.add(author.getId());
        }
        removeEntityStack.addAll(createdAuthors);

        List<Author> retrievedAuthors = authorDAO.findWhereIdIn(authorsIds);

        Collections.sort(createdAuthors, new EntityIdentifierComparator());
        Collections.sort(retrievedAuthors, new EntityIdentifierComparator());

        Assert.assertArrayEquals(createdAuthors.toArray(), retrievedAuthors.toArray());
    }

}
