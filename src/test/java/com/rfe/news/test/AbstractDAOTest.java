package com.rfe.news.test;

import com.rfe.news.entity.Author;
import com.rfe.news.entity.Comment;
import com.rfe.news.entity.News;
import com.rfe.news.entity.Tag;
import com.rfe.news.persistence.SimpleConnectionManager;
import com.rfe.news.persistence.dao.AuthorDAO;
import com.rfe.news.persistence.dao.CommentDAO;
import com.rfe.news.persistence.dao.NewsDAO;
import com.rfe.news.persistence.dao.TagDAO;
import com.rfe.news.persistence.dao.impl.AuthorDAOImpl;
import com.rfe.news.persistence.dao.impl.CommentDAOImpl;
import com.rfe.news.persistence.dao.impl.NewsDAOImpl;
import com.rfe.news.persistence.dao.impl.TagDAOImpl;
import com.rfe.news.persistence.dao.template.DefaultQueryTemplate;
import com.rfe.news.persistence.dao.template.QueryTemplate;
import com.rfe.news.test.utils.RemoveEntityStack;
import org.apache.commons.lang3.RandomStringUtils;

import java.sql.Timestamp;

public class AbstractDAOTest {

    protected QueryTemplate queryTemplate = DefaultQueryTemplate.getQueryTemplate();

    protected AuthorDAO authorDAO = new AuthorDAOImpl(SimpleConnectionManager.getConnectionManager());

    protected TagDAO tagDAO = TagDAOImpl.getTagDAO();

    protected NewsDAO newsDAO = NewsDAOImpl.getNewsDAO();

    protected CommentDAO commentDAO = CommentDAOImpl.getCommentDAO();

    protected RemoveEntityStack removeEntityStack = new RemoveEntityStack();

    protected Author buildAuthor() throws Exception {
        Author author = new Author();
        author.setAuthorName(randomString(10));
        author.setAuthorEmail(randomString(10));
        return author;
    }

    protected News buildNews() {
        News news = new News();
        news.setTitle(randomString(10));
        news.setBrief(randomString(30));
        news.setContent(randomString(100));
        news.setCreationDate(new Timestamp(System.nanoTime()));
        Author author = new Author();
        author.setAuthorName(randomString(10));
        author.setAuthorEmail(randomString(10));
        news.setAuthor(author);
        return news;
    }

    protected Tag buildTag() {
        Tag tag = new Tag();
        tag.setTagName(randomString(10));
        return tag;
    }

    protected Comment buildComment(Integer newsId) {
        Comment comment = new Comment();
        comment.setCommentText(randomString(100));
        comment.setCreationDate(new Timestamp(System.nanoTime()));
        comment.setNewsId(newsId);
        return comment;
    }

    protected String randomString(int count) {
        return RandomStringUtils.randomAlphabetic(count);
    }
}
