package com.rfe.news.test.utils;

import com.rfe.news.entity.DomainEntity;

import java.util.Comparator;

/**
 * Just compares the two Entities Identifiers, if they are Integer values.
 * Used only in tests.
 *
 * @author aleh_rusak
 */
public class EntityIdentifierComparator implements Comparator<DomainEntity<Integer>> {

    @Override
    public int compare(DomainEntity<Integer> e1, DomainEntity<Integer> e2) {
        return e1.getId() - e2.getId();
    }
}
