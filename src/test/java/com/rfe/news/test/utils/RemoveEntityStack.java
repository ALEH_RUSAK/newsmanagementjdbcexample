package com.rfe.news.test.utils;

import com.rfe.news.entity.Author;
import com.rfe.news.entity.Comment;
import com.rfe.news.entity.DomainEntity;
import com.rfe.news.entity.News;
import com.rfe.news.entity.Tag;
import com.rfe.news.exception.DAOException;
import com.rfe.news.persistence.SimpleConnectionManager;
import com.rfe.news.persistence.dao.CrudOperations;
import com.rfe.news.persistence.dao.impl.AuthorDAOImpl;
import com.rfe.news.persistence.dao.impl.CommentDAOImpl;
import com.rfe.news.persistence.dao.impl.NewsDAOImpl;
import com.rfe.news.persistence.dao.impl.TagDAOImpl;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Represents a collection (Map of grouped by type) of entities that supposed to be removed (deleted)
 * from the database after a @Test method.
 *
 * @author aleh_rusak
 */
public class RemoveEntityStack {

    /**
     * BE WARE OF ORDER OF ENTITIES CLASSES.
     */
    private static final List<Class<? extends DomainEntity>> orderedDomainClasses = Arrays.asList(
            Comment.class,
            Tag.class,
            News.class,
            Author.class
    );

    /**
     * Static final Mapping "Entity Classes" -> "Data Access Class".
     */
    private static final Map<Class<? extends DomainEntity>, CrudOperations> dataAccessObjects = new HashMap<>();

    /**
     * Static initializer. Used to initialize the Map above.
     */
    static {
        dataAccessObjects.put(Author.class, new AuthorDAOImpl(SimpleConnectionManager.getConnectionManager()));
        dataAccessObjects.put(News.class, NewsDAOImpl.getNewsDAO());
        dataAccessObjects.put(Comment.class, CommentDAOImpl.getCommentDAO());
        dataAccessObjects.put(Tag.class, TagDAOImpl.getTagDAO());
    }

    /**
     * An "Entity Class" -> "Entities Set" Mapping
     * Used to group Entities by their type.
     */
    private Map<Class<? extends DomainEntity>, Set<DomainEntity>> groupedEntities = new HashMap<>();

    /**
     * Creates RemoveEntityStack. Initializes <code>groupedEntities</code> Map with empty Sets
     */
    public RemoveEntityStack() {
        for (Class<? extends DomainEntity> entityClass : orderedDomainClasses) {
            groupedEntities.put(entityClass, new HashSet<>());
        }
    }

    /**
     * Adds an entity into stack.
     *
     * @param entity entity to add.
     */
    public void add(DomainEntity entity) {
        Set<DomainEntity> entitiesSet = groupedEntities.get(entity.getClass());
        if (entitiesSet != null) {
            entitiesSet.add(entity);
        }
    }

    /**
     * Adds a collection of entities into stack.
     *
     * @param entities entities to add.
     */
    public void addAll(Collection<? extends DomainEntity> entities) {
        for (DomainEntity entity : entities) {
            add(entity);
        }
    }

    /**
     * Deletes all entities from a stack in order of <code>orderedDomainClasses</code> list.
     *
     * @throws DAOException
     */
    public void deleteEntities() throws DAOException {
        for (Class<? extends DomainEntity> entityClass : orderedDomainClasses) {
            deleteEntitiesOfType(entityClass);
        }
    }

    /**
     * Deletes all entities from a stack of specified type.
     *
     * @throws DAOException
     */
    @SuppressWarnings("unchecked")
    public void deleteEntitiesOfType(Class<? extends DomainEntity> entityClass) throws DAOException {
        CrudOperations crudOperations = dataAccessObjects.get(entityClass);
        Iterator<DomainEntity> iterator = groupedEntities.get(entityClass).iterator();
        while (iterator.hasNext()) {
            DomainEntity entity = iterator.next();
            crudOperations.delete(entity.getId());
            iterator.remove();
        }
    }
}
