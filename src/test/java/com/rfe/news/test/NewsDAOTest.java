package com.rfe.news.test;

import com.rfe.news.entity.Comment;
import com.rfe.news.entity.News;
import com.rfe.news.entity.Tag;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class NewsDAOTest extends AbstractDAOTest {

    @After
    public void tearDown() throws Exception {
        removeEntityStack.deleteEntities();
    }

    @Test
    public void testCreateNews() throws Exception {
        News createdNews = newsDAO.create(buildNews());
        removeEntityStack.add(createdNews.getAuthor());
        removeEntityStack.add(createdNews);

        News newsRetrieved = newsDAO.read(createdNews.getId());

        Assert.assertEquals(createdNews, newsRetrieved);
        Assert.assertEquals(createdNews.getAuthor(), newsRetrieved.getAuthor());
    }

    @Test
    public void testDeleteNews() throws Exception {

        // Prepare Test Case
        // Create Tags for News
        List<Tag> createdTags = new ArrayList<>();
        List<Integer> tagIds = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Tag tag = buildTag();
            createdTags.add(tagDAO.create(tag));
            tagIds.add(tag.getId());
        }
        removeEntityStack.addAll(createdTags);

        // Create News
        News createdNews = newsDAO.create(buildNews());
        removeEntityStack.add(createdNews.getAuthor());
        removeEntityStack.add(createdNews);

        Integer newsId = createdNews.getId();

        tagDAO.bindTagsToNews(tagIds, newsId);

        for (int i = 0; i < 5; i++) {
            commentDAO.create(buildComment(newsId));
        }

        // Now, Delete News
        newsDAO.delete(newsId);

        // And check, if there are any links on deleted News
        List<Comment> newsComments = commentDAO.findCommentsForNews(newsId);
        List<Tag> newsTags = tagDAO.findTagsForNews(newsId);

        Assert.assertNotNull(newsComments);
        Assert.assertNotNull(newsTags);
        Assert.assertTrue(newsComments.isEmpty());
        Assert.assertTrue(newsTags.isEmpty());
        Assert.assertNull(newsDAO.read(newsId));
    }

}
