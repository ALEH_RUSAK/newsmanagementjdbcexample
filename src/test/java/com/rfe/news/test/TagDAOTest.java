package com.rfe.news.test;

import com.rfe.news.entity.Author;
import com.rfe.news.entity.News;
import com.rfe.news.entity.Tag;
import com.rfe.news.test.utils.EntityIdentifierComparator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TagDAOTest extends AbstractDAOTest {

    @After
    public void tearDown() throws Exception {
        removeEntityStack.deleteEntities();
    }

    @Test
    public void testGetTagsForNews() throws Exception {
        List<Tag> createdTags = new ArrayList<>();
        List<Integer> tagIds = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Tag tag = buildTag();
            createdTags.add(tagDAO.create(tag));
            tagIds.add(tag.getId());
        }
        removeEntityStack.addAll(createdTags);

        News createdNews = newsDAO.create(buildNews());
        removeEntityStack.add(createdNews.getAuthor());
        removeEntityStack.add(createdNews);

        Integer newsId = createdNews.getId();

        tagDAO.bindTagsToNews(tagIds, newsId);

        List<Tag> retrievedTags = tagDAO.findTagsForNews(newsId);

        Collections.sort(createdTags, new EntityIdentifierComparator());
        Collections.sort(retrievedTags, new EntityIdentifierComparator());

        Assert.assertArrayEquals(createdTags.toArray(), retrievedTags.toArray());
    }

    @Test
    public void testTagExists() throws Exception {
        Tag tag = tagDAO.create(buildTag());
        removeEntityStack.add(tag);

        Assert.assertTrue(queryTemplate.exists(tag));

        tagDAO.delete(tag.getId());

        Assert.assertTrue(!queryTemplate.exists(tag));
    }
}
