
--CASCADE DROP needed to drop schema recursively
--with all it's database objects (tables, indexes, constraints e.g.)
DROP SCHEMA IF EXISTS news_management CASCADE;

CREATE SCHEMA IF NOT EXISTS news_management;

--Create "news" table
CREATE TABLE news_management.news (
  title VARCHAR(200) NOT NULL,
  brief VARCHAR(500) NOT NULL,
  content TEXT NOT NULL,
  creation_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  author_name VARCHAR(50) NOT NULL,
  author_email VARCHAR(50),
  tags TEXT NOT NULL,
  comments VARCHAR(300),
  comments_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp
);

--Add primary key column to "news" table
ALTER TABLE news_management.news ADD COLUMN news_id INTEGER NOT NULL;
ALTER TABLE news_management.news ADD CONSTRAINT news_pkey PRIMARY KEY (news_id);

--Add Check-Constraint
--ALTER TABLE news_management.news ADD CHECK (creation_date <= current_timestamp);

--Add Unique-Constraint
ALTER TABLE news_management.news ADD CONSTRAINT title_unique UNIQUE (title);

--Create "comments" table
CREATE TABLE news_management.authors (
  author_id INTEGER PRIMARY KEY,
  author_name VARCHAR(50) NOT NULL UNIQUE,
  author_email VARCHAR(50)
);

--Drop unused columns of "news" table due to "authors" table has been created
ALTER TABLE news_management.news DROP COLUMN author_name;
ALTER TABLE news_management.news DROP COLUMN author_email;

--Create Many-To-One relation between "news" and "authors"
ALTER TABLE news_management.news ADD COLUMN author_id INTEGER;
ALTER TABLE news_management.news ADD CONSTRAINT news_authors_fk
FOREIGN KEY (author_id) REFERENCES news_management.authors (author_id);

--Create "comments" table
CREATE TABLE news_management.comments (
  comment_id INTEGER PRIMARY KEY,
  comment_text VARCHAR(200) NOT NULL,
  creation_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  news_id INTEGER NOT NULL
);

--Create Many-To-One relation between "comments" and "news"
ALTER TABLE news_management.comments ADD CONSTRAINT comments_news_fk
FOREIGN KEY (news_id) REFERENCES news_management.news (news_id);

--Drop unused columns of "news" table due to "comments" table has been created
ALTER TABLE news_management.news DROP COLUMN comments;
ALTER TABLE news_management.news DROP COLUMN comments_date;

--Create "tags" table
CREATE TABLE news_management.tags (
  tag_id INTEGER PRIMARY KEY,
  tag_name VARCHAR(30) NOT NULL DEFAULT 'About Everything And Nothing...'
);

--Drop unused columns of "news" table due to "tags" table has been created
ALTER TABLE news_management.news DROP COLUMN tags;

--Create Many-To-Many relation between "news" and "tags" with the help of table-connection
CREATE TABLE news_management.l_news_tags (
  news_tags_id INTEGER PRIMARY KEY,
  news_id INTEGER NOT NULL,
  tag_id INTEGER NOT NULL
);

ALTER TABLE news_management.l_news_tags ADD CONSTRAINT news_tags_fk1
FOREIGN KEY (news_id) REFERENCES news_management.news (news_id);

ALTER TABLE news_management.l_news_tags ADD CONSTRAINT news_tags_fk2
FOREIGN KEY (tag_id) REFERENCES news_management.tags (tag_id);

--Create Sequences to provide automatic Primary Key values generation with a nextval('schema_name.sequence_name') embedded function
CREATE SEQUENCE news_management.news_seq INCREMENT BY 1 MINVALUE 100 MAXVALUE 9999999 START WITH 100 NO CYCLE;
CREATE SEQUENCE news_management.tags_seq INCREMENT BY 1 MINVALUE 100 MAXVALUE 9999999 START WITH 100 NO CYCLE;
CREATE SEQUENCE news_management.authors_seq INCREMENT BY 1 MINVALUE 100 MAXVALUE 9999999 START WITH 100 NO CYCLE;
CREATE SEQUENCE news_management.comment_seq INCREMENT BY 1 MINVALUE 100 MAXVALUE 9999999 START WITH 100 NO CYCLE;
CREATE SEQUENCE news_management.l_news_tags_seq INCREMENT BY 1 MINVALUE 100 MAXVALUE 9999999 START WITH 100 NO CYCLE;