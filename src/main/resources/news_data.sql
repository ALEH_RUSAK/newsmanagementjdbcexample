-- DATA MANIPULATION LANGUAGE (DML)

-- TEST INSERTS

-- POPULATE AUTHORS TEST DATA
INSERT INTO news_management.authors (author_id, author_name, author_email)
VALUES (nextval('news_management.authors_seq'), 'Name1', 'Email1');

INSERT INTO news_management.authors (author_id, author_name, author_email)
VALUES (nextval('news_management.authors_seq'), 'Name2', 'Email2');

INSERT INTO news_management.authors (author_id, author_name, author_email)
VALUES (nextval('news_management.authors_seq'), 'Name3', 'Email3');

-- POPULATE NEWS TEST DATA
INSERT INTO news_management.news (news_id, title, brief, content, author_id)
VALUES (nextval('news_management.news_seq'), 'Title1', 'Brief1', 'Content1', 100);

INSERT INTO news_management.news (news_id, title, brief, content, author_id)
VALUES (nextval('news_management.news_seq'), 'Title2', 'Brief2', 'Content2', 101);

INSERT INTO news_management.news (news_id, title, brief, content, author_id)
VALUES (nextval('news_management.news_seq'), 'Title3', 'Brief3', 'Content3', 101);

INSERT INTO news_management.news (news_id, title, brief, content, author_id)
VALUES (nextval('news_management.news_seq'), 'Title4', 'Brief4', 'Content4', 102);

-- POPULATE COMMENTS TEST DATA
INSERT INTO news_management.comments (comment_id, comment_text, creation_date, news_id)
VALUES (nextval('news_management.comment_seq'), 'Comment1', '2016-11-10', 100);

INSERT INTO news_management.comments (comment_id, comment_text, creation_date, news_id)
VALUES (nextval('news_management.comment_seq'), 'Comment2', '2016-10-29', 100);

INSERT INTO news_management.comments (comment_id, comment_text, news_id)
VALUES (nextval('news_management.comment_seq'), 'Comment3', 101);

INSERT INTO news_management.comments (comment_id, comment_text, news_id)
VALUES (nextval('news_management.comment_seq'), 'Comment4', 101);

INSERT INTO news_management.comments (comment_id, comment_text, news_id)
VALUES (nextval('news_management.comment_seq'), 'Comment5', 101);

INSERT INTO news_management.comments (comment_id, comment_text, news_id)
VALUES (nextval('news_management.comment_seq'), 'Comment6', 102);


-- POPULATE TAGS TEST DATA
INSERT INTO news_management.tags (tag_id, tag_name)
VALUES (nextval('news_management.tags_seq'), 'Tag1');

INSERT INTO news_management.tags (tag_id, tag_name)
VALUES (nextval('news_management.tags_seq'), 'Tag2');

INSERT INTO news_management.tags (tag_id, tag_name)
VALUES (nextval('news_management.tags_seq'), 'Tag3');

INSERT INTO news_management.tags (tag_id, tag_name)
VALUES (nextval('news_management.tags_seq'), 'Tag4');

INSERT INTO news_management.tags (tag_id, tag_name)
VALUES (nextval('news_management.tags_seq'), 'Tag5');

INSERT INTO news_management.tags (tag_id, tag_name)
VALUES (nextval('news_management.tags_seq'), 'Tag6');

INSERT INTO news_management.tags (tag_id, tag_name)
VALUES (nextval('news_management.tags_seq'), 'Tag7');

INSERT INTO news_management.tags (tag_id, tag_name)
VALUES (nextval('news_management.tags_seq'), 'Tag8');


--POPULATE NEWS <-> TAGS MANY-TO-MANY RELATION DATA
INSERT INTO news_management.l_news_tags (news_tags_id, news_id, tag_id)
VALUES (nextval('news_management.l_news_tags_seq'), 100, 100);

INSERT INTO news_management.l_news_tags (news_tags_id, news_id, tag_id)
VALUES (nextval('news_management.l_news_tags_seq'), 100, 101);

INSERT INTO news_management.l_news_tags (news_tags_id, news_id, tag_id)
VALUES (nextval('news_management.l_news_tags_seq'), 100, 102);

INSERT INTO news_management.l_news_tags (news_tags_id, news_id, tag_id)
VALUES (nextval('news_management.l_news_tags_seq'), 101, 102);

INSERT INTO news_management.l_news_tags (news_tags_id, news_id, tag_id)
VALUES (nextval('news_management.l_news_tags_seq'), 102, 105);