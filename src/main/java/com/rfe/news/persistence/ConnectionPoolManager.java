package com.rfe.news.persistence;


import java.sql.Connection;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Extends the {@link AbstractConnectionManager} by providing
 * mechanism of threadsafe database Connections pooling.
 *
 * @author aleh_rusak
 *
 * TODO: IMPLEMENT YOUR OWN THREADSAFE CONNECTION POOL.
 * TODO: SEE THE USAGE EXAMPLES OF ArrayBlockingQueue
 */
public class ConnectionPoolManager extends AbstractConnectionManager {

    private boolean poolConnections;

    private int poolSize;

    /**
     * Concurrent collection for keeping Connections
     */
    private ArrayBlockingQueue<Connection> connectionPool;

    public ConnectionPoolManager() {
    }

    /**
     * Initializes Connection Pool.
     * Supposed to be used at <code>ConnectionPoolManager</code> construction.
     */
    public void initialize() {
        // Initialize pool with several already registered Connections
    }

    @Override
    public Connection openConnection() {
        return null;
    }

    @Override
    public void closeConnection(Connection connection) {
    }

    /**
     * Method provides destroying Connection Pool
     * That means to close all opened Connections and clear <code>connectionPool</code> collection
     */
    public void destroy() {
        // Destroy Pool
    }
}
