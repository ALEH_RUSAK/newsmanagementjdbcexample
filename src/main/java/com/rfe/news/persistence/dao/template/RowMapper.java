package com.rfe.news.persistence.dao.template;


import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Provides mapping the retrieved {@link ResultSet} to an object.
 * Subclasses provide specification of the type of result object to return.
 *
 * @author aleh_rusak
 */
public interface RowMapper<T> {

	/**
	 * Instantiates and initializes an object by given {@link ResultSet} data.
     * Subclasses should be aware of the type of result object, to prevent
     * Object-Relational-Mapping mismatch.
     *
	 * @param resultSet the {@link ResultSet}
	 * @throws SQLException 
	 */
	T mapRow(ResultSet resultSet) throws SQLException;
}
