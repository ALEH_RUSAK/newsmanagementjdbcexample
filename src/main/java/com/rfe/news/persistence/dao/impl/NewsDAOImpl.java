package com.rfe.news.persistence.dao.impl;

import com.rfe.news.entity.Author;
import com.rfe.news.entity.News;
import com.rfe.news.exception.DAOException;
import com.rfe.news.persistence.dao.NewsDAO;
import com.rfe.news.persistence.dao.template.PreparedStatementBuilder;
import com.rfe.news.persistence.dao.template.RowMapper;
import com.rfe.news.persistence.dao.template.TransactionExecutor;
import com.rfe.news.persistence.dao.util.DatabaseUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of {@link NewsDAO}.
 *
 * This is a Singleton class.
 *
 * @author aleh_rusak
 */
public class NewsDAOImpl extends AbstractDAO implements NewsDAO {

    private static final String NEWS_ID = "news_id";
    private static final String AUTHOR_ID = "author_id";

    private static final String SQL_SELECT_NEWS_BY_ID =
            "SELECT n.news_id, n.title, n.brief, n.content, n.creation_date, " +
            "a.author_id, a.author_name, a.author_email FROM news_management.news n " +
            "INNER JOIN news_management.authors a ON n.author_id = a.author_id " +
            "WHERE n.news_id = ?";

    private static final String SQL_INSERT_NEWS =
            "INSERT INTO news_management.news (news_id, title, brief, content, creation_date, author_id) VALUES (nextval('news_management.news_seq'),?,?,?,?,?)";

    private static final String SQL_INSERT_NEWS_AUTHOR =
            "INSERT INTO news_management.authors (author_id, author_name, author_email) VALUES (nextval('news_management.authors_seq'),?,?)";

    private static final String SQL_UNBIND_TAGS_FROM_NEWS =
            "DELETE FROM news_management.l_news_tags WHERE news_id = ?";

    private static final String SQL_DELETE_NEWS_COMMENTS =
            "DELETE FROM news_management.comments WHERE news_id = ?";

    private static final String SQL_DELETE_NEWS =
            "DELETE FROM news_management.news WHERE news_id = ?";

    private static final String SQL_SELECT_NEWS_BY_AUTHOR =
            "SELECT n.news_id, n.title, n.brief, n.content, n.creation_date, " +
            "a.author_id, a.author_name, a.author_email FROM news_management.news n" +
            "INNER JOIN news_management.authors a ON n.author_id = a.author_id " +
            "WHERE a.author_id = ?";

    private static final String SQL_SELECT_NEWS_BY_TAGS =
            "SELECT DISTINCT ON (n.news_id) n.news_id, n.title, n.brief, n.content, n.creation_date, " +
            "a.author_id, a.author_name, a.author_email FROM news_management.news n " +
            "LEFT JOIN news_management.authors a ON n.author_id = a.author_id " +
            "INNER JOIN news_management.l_news_tags l ON l.news_id = n.news_id " +
            "INNER JOIN news_management.tags t ON t.tag_id = l.tag_id " +
            "WHERE t.tag_id ";

    private RowMapper<News> newsRowMapper = new NewsRowMapper();

    private PreparedStatementBuilder<News> newsPreparedStatementBuilder = new NewsPreparedStatementBuilder();

    private PreparedStatementBuilder<Author> authorPreparedStatementBuilder = new AuthorPreparedStatementBuilder();

    private static NewsDAO newsDAO;

    public static NewsDAO getNewsDAO() {
        if (newsDAO == null) {
            newsDAO = new NewsDAOImpl();
        }
        return newsDAO;
    }

    private NewsDAOImpl() {
    }

    @Override
    @SuppressWarnings("unchecked")
    public News create(News news) throws DAOException {
        TransactionExecutor transaction = new TransactionExecutor(connectionManager);
        try {
            // Start a Transaction
            transaction.start();

            // Do operations
            Author author = news.getAuthor();
            Integer authorId = transaction.create(author, SQL_INSERT_NEWS_AUTHOR, AUTHOR_ID, authorPreparedStatementBuilder);
            author.setId(authorId);

            Integer newsId = transaction.create(news, SQL_INSERT_NEWS, NEWS_ID, newsPreparedStatementBuilder);
            news.setId(newsId);

            // Commit a Transaction
            transaction.commit();
        } catch (Throwable e) {

            // Rollback, if Exception was thrown
            transaction.rollback();
            throw new DAOException(e);
        }
        return news;
    }

    @Override
    public News read(Integer newsId) throws DAOException {
        return queryTemplate.read(newsId, SQL_SELECT_NEWS_BY_ID, newsRowMapper);
    }

    @Override
    public void delete(Integer newsId) throws DAOException {
        TransactionExecutor transaction = new TransactionExecutor(connectionManager);
        try {
            transaction.start();

            // First, delete all Foreign Key references, e.g. comments and tags.
            transaction.delete(newsId, SQL_DELETE_NEWS_COMMENTS);
            transaction.delete(newsId, SQL_UNBIND_TAGS_FROM_NEWS);

            // And then we can safety delete news.
            transaction.delete(newsId, SQL_DELETE_NEWS);

            transaction.commit();
        } catch (Throwable e) {
            transaction.rollback();
            throw new DAOException(e);
        }
    }

    @Override
    public List<News> findByAuthor(Integer authorId) throws DAOException {
        return queryTemplate.queryForList(SQL_SELECT_NEWS_BY_AUTHOR, new Object[] {authorId}, newsRowMapper);
    }

    @Override
    public List<News> findByTags(List<Integer> tagIds) throws DAOException {
        if (tagIds == null || tagIds.isEmpty()) {
            return new ArrayList<>();
        }
        String query = SQL_SELECT_NEWS_BY_TAGS + DatabaseUtils.buildPreparedStatementWhereInClause(tagIds.size());
        return queryTemplate.queryForList(query, tagIds.toArray(), newsRowMapper);
    }

    @Override
    public News update(News news) throws DAOException {
        // TODO: IMPLEMENT
        return null;
    }

    /**
     * Provides implementation for {@link RowMapper} for News entity.
     *
     * @author aleh_rusak
     */
    private class NewsRowMapper implements RowMapper<News> {

        @Override
        @SuppressWarnings("unchecked")
        public News mapRow(ResultSet resultSet) throws SQLException {
            News news = new News();
            news.setId(resultSet.getInt("news_id"));
            news.setTitle(resultSet.getString("title"));
            news.setBrief(resultSet.getString("brief"));
            news.setContent(resultSet.getString("content"));
            news.setCreationDate(resultSet.getTimestamp("creation_date"));
            Author author = new Author();
            author.setId(resultSet.getInt("author_id"));
            author.setAuthorName(resultSet.getString("author_name"));
            author.setAuthorEmail(resultSet.getString("author_email"));
            news.setAuthor(author);
            return news;
        }
    }

    /**
     * Provides implementation of {@link PreparedStatementBuilder} for News entity
     *
     * @author aleh_rusak
     */
    private class NewsPreparedStatementBuilder implements PreparedStatementBuilder<News> {

        @Override
        public void buildPreparedStatement(News news, PreparedStatement preparedStatement) throws SQLException {
            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getBrief());
            preparedStatement.setString(3, news.getContent());
            preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
            preparedStatement.setInt(5, news.getAuthor().getId());
        }
    }

    /**
     * Provides implementation of {@link PreparedStatementBuilder} for Author entity
     *
     * @author aleh_rusak
     */
    private class AuthorPreparedStatementBuilder implements PreparedStatementBuilder<Author> {

        @Override
        public void buildPreparedStatement(Author author, PreparedStatement preparedStatement) throws SQLException {
            preparedStatement.setString(1, author.getAuthorName());
            preparedStatement.setString(2, author.getAuthorEmail());
        }
    }
}
