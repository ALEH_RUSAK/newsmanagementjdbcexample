package com.rfe.news.persistence.dao.impl;

import com.rfe.news.entity.Tag;
import com.rfe.news.exception.DAOException;
import com.rfe.news.persistence.dao.TagDAO;
import com.rfe.news.persistence.dao.template.PreparedStatementBuilder;
import com.rfe.news.persistence.dao.template.RowMapper;
import com.rfe.news.persistence.dao.template.TransactionExecutor;
import com.rfe.news.persistence.dao.util.DatabaseResourcesManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

/**
 * Default implementation of {@link TagDAO}
 *
 * This is a Singleton class.
 *
 * @author aleh_rusak
 */
public class TagDAOImpl extends AbstractDAO implements TagDAO {

    private static final String TAG_ID = "tag_id";

    private static final String SQL_INSERT_TAG =
            "INSERT INTO news_management.tags (tag_id, tag_name) VALUES (nextval('news_management.tags_seq'),?)";

    private static final String SQL_SELECT_TAG_BY_ID =
            "SELECT tag_id, tag_name FROM news_management.tags WHERE tag_id = ?";

    private static final String SQL_UPDATE_TAG =
            "UPDATE news_management.tags SET tag_name = ? WHERE tag_id = ?";

    private static final String SQL_SELECT_TAG_BY_NAME =
            "SELECT tag_id, tag_name FROM news_management.tags WHERE tag_name = ?";

    private static final String SQL_DELETE_TAG =
            "DELETE FROM news_management.tags WHERE tag_id = ?";

    private static final String SQL_UNBIND_TAGS_FROM_NEWS =
            "DELETE FROM news_management.l_news_tags WHERE tag_id = ?";

    private static final String SQL_BIND_TAGS_TO_NEWS =
            "INSERT INTO news_management.l_news_tags (news_tags_id, news_id, tag_id) VALUES (nextval('news_management.l_news_tags_seq'),?,?)";

    private static final String SQL_SELECT_TAGS_FOR_NEWS =
            "SELECT t.tag_id, t.tag_name FROM news_management.tags t " +
            "INNER JOIN news_management.l_news_tags l ON t.tag_id = l.tag_id " +
            "INNER JOIN news_management.news n ON l.news_id = n.news_id " +
            "WHERE n.news_id = ?";

    private RowMapper<Tag> tagRowMapper = new TagRowMapper();

    private PreparedStatementBuilder<Tag> tagPreparedStatementBuilder = new TagPreparedStatementBuilder();

    private static TagDAO tagDAO;

    public static TagDAO getTagDAO() {
        if (tagDAO == null) {
            tagDAO = new TagDAOImpl();
        }
        return tagDAO;
    }

    private TagDAOImpl() {
    }

    @Override
    public Tag create(Tag tag) throws DAOException {
        Integer generatedId = queryTemplate.create(tag, SQL_INSERT_TAG, TAG_ID, tagPreparedStatementBuilder);
        tag.setId(generatedId);
        return tag;
    }

    @Override
    public Tag read(Integer tagId) throws DAOException {
        return queryTemplate.read(tagId, SQL_SELECT_TAG_BY_ID, tagRowMapper);
    }

    @Override
    public Tag update(Tag tag) throws DAOException {
        return queryTemplate.update(tag, tag.getId(), SQL_UPDATE_TAG, tagPreparedStatementBuilder);
    }

    @Override
    public void delete(Integer tagId) throws DAOException {
        TransactionExecutor transaction = new TransactionExecutor(connectionManager);
        try {
            // Start a Transaction
            transaction.start();

            // First, delete all Foreign Key references from "news_management.l_news_tags"
            transaction.delete(tagId, SQL_UNBIND_TAGS_FROM_NEWS);

            // Then, delete Tag
            transaction.delete(tagId, SQL_DELETE_TAG);

            transaction.commit();
        } catch (Throwable e) {
            transaction.rollback();
            throw new DAOException(e);
        }
    }

    /**
     * It is important to keep in mind, that each update added to a Statement or PreparedStatement
     * is executed separately by the database.
     * That means, that some of them may succeed before one of them fails.
     * All the statements that have succeeded are now applied to the database,
     * but the rest of the updates may not be.
     * This can result in an inconsistent data in the database.
     *
     * To avoid this, you should execute the batch update inside a Transaction.
     *
     * @param tagIds IDs of Tags
     * @param newsId ID of the News
     * @throws DAOException
     */
    @Override
    public void bindTagsToNews(Collection<Integer> tagIds, Integer newsId) throws DAOException {
        PreparedStatement preparedStatement = null;
        TransactionExecutor transaction = new TransactionExecutor(connectionManager);
        try {
            transaction.start();
            preparedStatement = transaction.getConnection().prepareStatement(SQL_BIND_TAGS_TO_NEWS);
            for (Integer tagId: tagIds) {
                preparedStatement.setInt(1, newsId);
                preparedStatement.setInt(2, tagId);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            transaction.commit();
        } catch (Throwable e) {
            transaction.rollback();
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, null);
        }
    }

    @Override
    public List<Tag> findTagsForNews(Integer newsId) throws DAOException {
        return queryTemplate.queryForList(SQL_SELECT_TAGS_FOR_NEWS, new Object[] {newsId}, tagRowMapper);
    }

    @Override
    public Tag findByName(String tagName) throws DAOException {
        return queryTemplate.queryForObject(SQL_SELECT_TAG_BY_NAME, new Object[] {tagName}, tagRowMapper);
    }

    /**
     * Provides implementation of {@link RowMapper} for Tag entity
     *
     * @author aleh_rusak
     */
    private class TagRowMapper implements RowMapper<Tag> {

        @Override
        public Tag mapRow(ResultSet resultSet) throws SQLException {
            Tag tag = new Tag();
            tag.setId(resultSet.getInt(1));
            tag.setTagName(resultSet.getString(2));
            return tag;
        }
    }

    /**
     * Provides implementation of {@link PreparedStatementBuilder} for Tag entity
     *
     * @author aleh_rusak
     */
    private class TagPreparedStatementBuilder implements PreparedStatementBuilder<Tag> {

        @Override
        public void buildPreparedStatement(Tag tag, PreparedStatement preparedStatement) throws SQLException {
            preparedStatement.setString(1, tag.getTagName());
        }
    }
}
