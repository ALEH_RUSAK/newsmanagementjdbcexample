package com.rfe.news.persistence.dao.impl;

import com.rfe.news.persistence.AbstractConnectionManager;
import com.rfe.news.persistence.dao.template.DefaultQueryTemplate;
import com.rfe.news.persistence.dao.template.QueryTemplate;

public abstract class AbstractDAO {

    protected QueryTemplate queryTemplate = DefaultQueryTemplate.getQueryTemplate();

    protected AbstractConnectionManager connectionManager = queryTemplate.getConnectionManager();
}
