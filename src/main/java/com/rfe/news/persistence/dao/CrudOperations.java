package com.rfe.news.persistence.dao;

import com.rfe.news.entity.DomainEntity;
import com.rfe.news.exception.DAOException;

import java.io.Serializable;

/**
 * Provides CRUD (Create, Read, Update, Delete) abstraction for Data Access Object layer.
 *
 * T extends DomainEntity<ID> means, that sub-classes may specify type with which they will work.
 *
 * @author aleh_rusak
 */
public interface CrudOperations<T extends DomainEntity<ID>, ID extends Serializable> {
	
	/** 
	 * Saves new entity into database.
     *
	 * @return already saved entity
	 */
    T create(T entity) throws DAOException;

	/** 
     * Retrieves entity by it's ID.
     *
     * @param entityId the PK value
     * @return entity
     */
	T read(ID entityId) throws DAOException;

    /**
     * Updates entity with new field values.
     *
     * @param entity new entity state to update databases one
     */
    T update(T entity) throws DAOException;

    /**
     * Deletes entity from database by given ID.
     *
     * @param entityId row with this ID value will be deleted from a database
     */
    void delete(ID entityId) throws DAOException;
}
