package com.rfe.news.persistence.dao;


import com.rfe.news.entity.Comment;
import com.rfe.news.exception.DAOException;

import java.util.List;

/**
 * <code>CrudOperations<T, ID></code> extension for operations with <code>Comment</code> entity
 *
 * @author aleh_rusak
 */
public interface CommentDAO extends CrudOperations<Comment, Integer> {
	
	/**
	 * Getting list of News Comments.
     *
	 * @param newsId the ID of the News
	 * @return the list of News Comments
	 * @throws DAOException
	 */
	List<Comment> findCommentsForNews(Integer newsId) throws DAOException;
}
