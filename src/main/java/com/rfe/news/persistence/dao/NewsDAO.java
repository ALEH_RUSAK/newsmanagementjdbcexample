package com.rfe.news.persistence.dao;

import com.rfe.news.entity.News;
import com.rfe.news.exception.DAOException;

import java.util.List;

/**
 * {@link CrudOperations} extension for operations related to News entity
 *
 * @author aleh_rusak
 */
public interface NewsDAO extends CrudOperations<News, Integer> {
	
	/**
	 * Getting list of News by given authorId
     *
	 * @param authorId the ID of the Author
	 * @return list of News written by Author
	 * @throws DAOException
	 */
	List<News> findByAuthor(Integer authorId) throws DAOException;
	
	/**
	 * Getting list of News by given tags IDs
	 * @param tagIds the list of tags IDs
	 * @return list of News with Tags
	 * @throws DAOException
	 */
	List<News> findByTags(List<Integer> tagIds) throws DAOException;
}
