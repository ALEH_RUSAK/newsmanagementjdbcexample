package com.rfe.news.persistence.dao;

import com.rfe.news.entity.Author;
import com.rfe.news.exception.DAOException;

import java.util.Collection;
import java.util.List;

/**
 * {@link CrudOperations} extension for operations with Author entity
 *
 * @author aleh_rusak
 */
public interface AuthorDAO extends CrudOperations<Author, Integer> {

    /**
     * Returns a list of Authors by their IDs
     *
     * @param authorIds a collection of Authors IDs
     * @return list of Authors by their IDs
     */
    List<Author> findWhereIdIn(Collection<Integer> authorIds) throws DAOException;

	/**
	 * Getting author of news
	 * @param newsId news ID
	 * @return list of news authors
	 * @throws DAOException
	 */
	Author findAuthorForNews(Integer newsId) throws DAOException;
	
	/**
	 * Finds author by his name
	 * @param authorName name of the author
	 * @return <code>AuthorTO</code> instance
	 * @throws DAOException
	 */
	Author findByName(String authorName) throws DAOException;
}
