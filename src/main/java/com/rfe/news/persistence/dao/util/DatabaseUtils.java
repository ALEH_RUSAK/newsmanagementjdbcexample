package com.rfe.news.persistence.dao.util;

import com.rfe.news.exception.DAOException;
import com.rfe.news.persistence.annotation.Column;
import com.rfe.news.persistence.annotation.Identifier;
import com.rfe.news.persistence.annotation.Table;
import com.rfe.news.persistence.dao.template.RowMapper;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Utility class, where DAO help-methods should be placed.
 *
 * @author aleh_rusak
 */
public class DatabaseUtils {

    public static final String SELECT_WHERE_TEMPLATE = "SELECT %s FROM %s WHERE %s = ?";

    /**
     * Builds a "IN (value1, value2, ..., valueN)" string clause for appending it to a WHERE.
     *
     * @param parameters values to be placed in a clause
     * @return "IN (value1, value2, ..., valueN)" string
     */
    public static String buildWhereInClause(Object[] parameters) {
        StringBuilder whereInBuilder = new StringBuilder();
        whereInBuilder.append(" IN ( ");

        // Here Stream API is used.
        // Stream API and Lambda expressions are supported only by Java 8.
        // Great article about Stream API: https://habrahabr.ru/company/luxoft/blog/270383/
        Stream.of(parameters).filter(p -> p != null)
                .forEach(p -> {
                    whereInBuilder.append(p.toString());
                    whereInBuilder.append(" , ");
                });
        whereInBuilder.deleteCharAt(whereInBuilder.length() - 1);
        whereInBuilder.append(" ) ");
        return whereInBuilder.toString();
    }

    /**
     * Builds a "IN (?, ?, ..., ?)" string clause for appending it to a WHERE.
     *
     * @param parametersCount the number of supposed parameters in a {@link PreparedStatement}
     * @return "IN (?, ?, ..., ?)" string clause
     */
    public static String buildPreparedStatementWhereInClause(int parametersCount) {
        StringBuilder whereInBuilder = new StringBuilder();
        whereInBuilder.append(" IN ( ");
        for (int index = 0; index < parametersCount; index++) {
            whereInBuilder.append(" ? ,");
        }
        whereInBuilder.deleteCharAt(whereInBuilder.length() - 1);
        whereInBuilder.append(" ) ");
        return whereInBuilder.toString();
    }

    /**
     * Sets all parameter values into a {@link PreparedStatement},
     * if given parameters array is not empty.
     *
     * @param preparedStatement the {@link PreparedStatement}
     * @param parameters values to be set into a statement.
     * @throws SQLException
     */
    public static void fillInPreparedStatement(PreparedStatement preparedStatement, final Object[] parameters) throws SQLException {
        if (parameters == null || parameters.length == 0) {
            return;
        }
        for (int index = 1; index <= parameters.length; index++) {
            preparedStatement.setObject(index, parameters[index - 1]);
        }
    }

    /**
     * Sets all parameter values into a {@link PreparedStatement},
     * if given parameters Collection is not empty.
     *
     * @param preparedStatement the {@link PreparedStatement}
     * @param parameters Collection which values should be set into Statement.
     * @throws SQLException
     */
    public static void fillInPreparedStatement(PreparedStatement preparedStatement, final Collection<Object> parameters) throws SQLException {
        if (parameters == null || parameters.isEmpty()) {
            return;
        }
        int index = 1;
        for (Object param : parameters) {
            preparedStatement.setObject(index, param);
            index++;
        }
    }

    /**
     * Provides converting a {@link ResultSet} of database selection results
     * into a List of objects of specified type.
     *
     * @param resultSet the {@link ResultSet}
     * @param rowMapper the {@link RowMapper}
     * @return a list of objects
     * @throws SQLException
     */
    public static <T> List<T> getResultsList(ResultSet resultSet, RowMapper<T> rowMapper) throws SQLException {
        List<T> results = new ArrayList<>();
        while (resultSet.next()) {
            results.add(rowMapper.mapRow(resultSet));
        }
        return results;
    }

    @SuppressWarnings("unchecked")
    public static <T> Query buildExistenceQuery(T entity) throws DAOException {
        Query query = new Query();
        Class<T> entityClass = (Class<T>) entity.getClass();
        try {
            if (entityClass.isAnnotationPresent(Table.class)) {
                Field idField = getIdentifierField(entityClass.getDeclaredFields());
                String idName = idField.getAnnotation(Column.class).value();
                Object idValue = idField.get(entity);
                String tableName = entityClass.getAnnotation(Table.class).value();
                query.setSqlTemplate(String.format(SELECT_WHERE_TEMPLATE, idName, tableName, idName));
                query.setParameters(Collections.singletonList(idValue));
            }
        } catch (IllegalAccessException e) {
            throw new DAOException(e);
        }
        return query;
    }

    private static Field getIdentifierField(Field[] entityFields) throws DAOException {
        for (Field field : entityFields) {
            if (field.isAnnotationPresent(Identifier.class) && field.isAnnotationPresent(Column.class)) {
                field.setAccessible(true);
                return field;
            }
        }
        throw new DAOException("Can't find @Identifier. There is no @Identifier field or such field is not a @Column.");
    }

    private DatabaseUtils() {
    }
}
