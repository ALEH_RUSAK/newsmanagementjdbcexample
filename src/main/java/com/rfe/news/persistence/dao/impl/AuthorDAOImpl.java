package com.rfe.news.persistence.dao.impl;

import com.rfe.news.entity.Author;
import com.rfe.news.exception.DAOException;
import com.rfe.news.persistence.AbstractConnectionManager;
import com.rfe.news.persistence.dao.AuthorDAO;
import com.rfe.news.persistence.dao.util.DatabaseResourcesManager;
import com.rfe.news.persistence.dao.util.DatabaseUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Default implementation of {@link AuthorDAO}.
 *
 * @author aleh_rusak
 */
public class AuthorDAOImpl implements AuthorDAO {

    private static final String AUTHOR_ID = "author_id";

    private static final String SQL_INSERT_AUTHOR =
            "INSERT INTO news_management.authors (author_id, author_name, author_email) VALUES (nextval('news_management.authors_seq'), ?, ?)";

    private static final String SQL_SELECT_AUTHOR_BY_ID =
            "SELECT author_id, author_name, author_email FROM news_management.authors WHERE author_id = ?";

    private static final String SQL_UPDATE_AUTHOR =
            "UPDATE news_management.authors SET author_name = ?, author_email = ? WHERE author_id = ?";

    private static final String SQL_DELETE_AUTHOR =
            "DELETE FROM news_management.authors WHERE author_id = ?";

    private static final String SQL_SELECT_AUTHORS_WHERE_ID_IN =
            "SELECT author_id, author_name, author_email FROM news_management.authors " +
            "WHERE author_id ";

    private AbstractConnectionManager connectionManager;

    public AuthorDAOImpl(AbstractConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Author create(Author author) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = connectionManager.openConnection();
            preparedStatement = connection.prepareStatement(SQL_INSERT_AUTHOR, new String[] {AUTHOR_ID});
            preparedStatement.setString(1, author.getAuthorName());
            preparedStatement.setString(2, author.getAuthorEmail());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                author.setId(resultSet.getInt(AUTHOR_ID));
            } else {
                throw new DAOException("Exception while trying to insert Author.");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, resultSet);
            connectionManager.closeConnection(connection);
        }
        return author;
    }

    @Override
    public Author read(Integer authorId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = connectionManager.openConnection();
            preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_ID);
            preparedStatement.setInt(1, authorId);
            resultSet = preparedStatement.executeQuery();
            return resultSet.next() ? mapRow(resultSet) : null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, resultSet);
            connectionManager.closeConnection(connection);
        }
    }

    @Override
    public Author update(Author author) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionManager.openConnection();
            preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
            preparedStatement.setString(1, author.getAuthorName());
            preparedStatement.setString(2, author.getAuthorEmail());
            preparedStatement.setInt(3, author.getId());
            preparedStatement.executeUpdate();
            return author;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, null);
            connectionManager.closeConnection(connection);
        }
    }

    @Override
    public void delete(Integer authorId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionManager.openConnection();
            preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR);
            preparedStatement.setInt(1, authorId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, null);
            connectionManager.closeConnection(connection);
        }
    }

    @Override
    public List<Author> findWhereIdIn(Collection<Integer> authorIds) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Author> authors = new ArrayList<>();
        try {
            connection = connectionManager.openConnection();

            String whereInClause = DatabaseUtils.buildPreparedStatementWhereInClause(authorIds.size());
            preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHORS_WHERE_ID_IN + whereInClause);
            DatabaseUtils.fillInPreparedStatement(preparedStatement, authorIds.toArray());

            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                authors.add(mapRow(resultSet));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, resultSet);
            connectionManager.closeConnection(connection);
        }
        return authors;
    }

    @Override
    public Author findAuthorForNews(Integer newsId) throws DAOException {
        // TODO: IMPLEMENT
        return null;
    }

    @Override
    public Author findByName(String authorName) throws DAOException {
        // TODO: IMPLEMENT
        return null;
    }

    private Author mapRow(ResultSet resultSet) throws SQLException {
        Author author = new Author();
        author.setId(resultSet.getInt("author_id"));
        author.setAuthorName(resultSet.getString("author_name"));
        author.setAuthorEmail(resultSet.getString("author_email"));
        return author;
    }
}
