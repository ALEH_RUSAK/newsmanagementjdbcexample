package com.rfe.news.persistence.dao.template;

import com.rfe.news.exception.DAOException;
import com.rfe.news.persistence.AbstractConnectionManager;
import com.rfe.news.persistence.SimpleConnectionManager;
import com.rfe.news.persistence.dao.util.DatabaseResourcesManager;
import com.rfe.news.persistence.dao.util.DatabaseUtils;
import com.rfe.news.persistence.dao.util.Query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;


/**
 * The default {@link QueryTemplate} implementation.
 *
 * This is a Singleton class.
 *
 * @author aleh_rusak
 */
public class DefaultQueryTemplate implements QueryTemplate {

    private static DefaultQueryTemplate queryTemplate;

    public static DefaultQueryTemplate getQueryTemplate() {
        if (queryTemplate == null) {
            queryTemplate = new DefaultQueryTemplate();
        }
        return queryTemplate;
    }

    /**
     * {@link QueryTemplate} must be provided with a Connection Manager.
     */
    private AbstractConnectionManager connectionManager = SimpleConnectionManager.getConnectionManager();

    private DefaultQueryTemplate() {
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T, ID> ID create(T entity, String sqlQuery, String idColumn, PreparedStatementBuilder<T> statementMapper) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = connectionManager.openConnection();
            preparedStatement = connection.prepareStatement(sqlQuery, new String[] {idColumn});
            statementMapper.buildPreparedStatement(entity, preparedStatement);
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                return (ID) resultSet.getObject(idColumn);
            } else {
                throw new DAOException("Entity was not saved to database.");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, resultSet);
            connectionManager.closeConnection(connection);
        }
    }

    @Override
    public <T, ID> T read(ID entityId, String sqlQuery, RowMapper<T> rowMapper) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = connectionManager.openConnection();
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setObject(1, entityId);
            resultSet = preparedStatement.executeQuery();
            return resultSet.next() ? rowMapper.mapRow(resultSet) : null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, resultSet);
            connectionManager.closeConnection(connection);
        }
    }

    @Override
    public <T, ID> T update(T entity, ID entityId, String sqlQuery, PreparedStatementBuilder<T> statementMapper) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionManager.openConnection();
            preparedStatement = connection.prepareStatement(sqlQuery);
            statementMapper.buildPreparedStatement(entity, preparedStatement);
            int paramsCount = preparedStatement.getParameterMetaData().getParameterCount();
            preparedStatement.setObject(paramsCount, entityId);
            preparedStatement.executeUpdate();
            return entity;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, null);
            connectionManager.closeConnection(connection);
        }
    }

    @Override
    public <ID> void delete(ID entityId, String sqlQuery) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionManager.openConnection();
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setObject(1, entityId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, null);
            connectionManager.closeConnection(connection);
        }
    }

    @Override
    public boolean executeQuery(String sqlQuery) throws DAOException {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = connectionManager.openConnection();
            statement = connection.createStatement();
            return statement.execute(sqlQuery);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(statement, null);
            connectionManager.closeConnection(connection);
        }
    }

    @Override
    public <T> T queryForObject(final String sqlQuery, final Object[] queryParameters, RowMapper<T> rowMapper) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = connectionManager.openConnection();
            preparedStatement = connection.prepareStatement(sqlQuery);
            DatabaseUtils.fillInPreparedStatement(preparedStatement, queryParameters);
            resultSet = preparedStatement.executeQuery();
            return resultSet.next() ? rowMapper.mapRow(resultSet) : null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, resultSet);
            connectionManager.closeConnection(connection);
        }
    }

    @Override
    public <T> List<T> queryForList(final String sqlQuery, final Object[] queryParameters, RowMapper<T> rowMapper) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = connectionManager.openConnection();
            preparedStatement = connection.prepareStatement(sqlQuery);
            DatabaseUtils.fillInPreparedStatement(preparedStatement, queryParameters);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getResultsList(resultSet, rowMapper);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, resultSet);
            connectionManager.closeConnection(connection);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> boolean exists(T entity) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = connectionManager.openConnection();
            Query query = DatabaseUtils.buildExistenceQuery(entity);
            preparedStatement = connection.prepareStatement(query.getSqlTemplate());
            DatabaseUtils.fillInPreparedStatement(preparedStatement, query.getParameters());
            resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, resultSet);
            connectionManager.closeConnection(connection);
        }
    }

    @Override
    public AbstractConnectionManager getConnectionManager() {
        return connectionManager;
    }
}
