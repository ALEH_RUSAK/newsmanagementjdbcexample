package com.rfe.news.persistence.dao.template;


import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Provides building of {@link PreparedStatement} with the given entity object.
 *
 * @author aleh_rusak
 */
public interface PreparedStatementBuilder<T> {
	
	/**
	 * Initializes the given {@link PreparedStatement} with the fields of given object.
     *
     * @throws SQLException
	 */
    void buildPreparedStatement(T entity, PreparedStatement preparedStatement) throws SQLException;
}
