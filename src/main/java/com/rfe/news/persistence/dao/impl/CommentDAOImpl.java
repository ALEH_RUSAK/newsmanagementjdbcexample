package com.rfe.news.persistence.dao.impl;

import com.rfe.news.entity.Comment;
import com.rfe.news.exception.DAOException;
import com.rfe.news.persistence.dao.CommentDAO;
import com.rfe.news.persistence.dao.template.PreparedStatementBuilder;
import com.rfe.news.persistence.dao.template.RowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

/**
 * Default implementation of {@link CommentDAO}
 *
 * @author aleh_rusak
 */
public class CommentDAOImpl extends AbstractDAO implements CommentDAO {

    private static final String COMMENT_ID = "comment_id";

    private static final String SQL_INSERT_COMMENT =
            "INSERT INTO news_management.comments (comment_id, comment_text, creation_date, news_id) " +
            "VALUES (nextval('news_management.comment_seq'), ?, ?, ?)";

    private static final String SQL_SELECT_COMMENT_BY_ID =
            "SELECT comment_id, comment_text, creation_date, news_id FROM news_management.comments " +
            "WHERE comment_id = ?";

    private static final String SQL_FIND_COMMENTS_FOR_NEWS =
            "SELECT comment_id, comment_text, creation_date, news_id FROM news_management.comments " +
            "WHERE news_id = ?";

    private static final String SQL_DELETE_COMMENT = "DELETE FROM news_management.comments WHERE comment_id = ?";

    private RowMapper<Comment> commentRowMapper = new CommentRowMapper();

    private PreparedStatementBuilder<Comment> commentPreparedStatementBuilder = new CommentPreparedStatementBuilder();

    private static CommentDAO commentDAO;

    public static CommentDAO getCommentDAO() {
        if (commentDAO == null) {
            commentDAO = new CommentDAOImpl();
        }
        return commentDAO;
    }

    private CommentDAOImpl() {
    }

    @Override
    public Comment create(Comment comment) throws DAOException {
        Integer commentId = queryTemplate.create(comment, SQL_INSERT_COMMENT, COMMENT_ID, commentPreparedStatementBuilder);
        comment.setId(commentId);
        return comment;
    }

    @Override
    public Comment read(Integer commentId) throws DAOException {
        return queryTemplate.read(commentId, SQL_SELECT_COMMENT_BY_ID, commentRowMapper);
    }

    @Override
    public void delete(Integer commentId) throws DAOException {
        queryTemplate.delete(commentId, SQL_DELETE_COMMENT);
    }

    @Override
    public List<Comment> findCommentsForNews(Integer newsId) throws DAOException {
        return queryTemplate.queryForList(SQL_FIND_COMMENTS_FOR_NEWS, new Object[] {newsId}, new CommentRowMapper());
    }

    @Override
    public Comment update(Comment comment) throws DAOException {
        // TODO: IMPLEMENT, MOTHERFUCKERS
        return null;
    }

    /**
     * Provides implementation for <code>RowMapper</code> for <code>Comment</code> entity.
     *
     * @author aleh_rusak
     */
    private class CommentRowMapper implements RowMapper<Comment> {

        @Override
        public Comment mapRow(ResultSet resultSet) throws SQLException {
            Comment comment = new Comment();
            comment.setId(resultSet.getInt("comment_id"));
            comment.setCommentText(resultSet.getString("comment_text"));
            comment.setCreationDate(resultSet.getTimestamp("creation_date"));
            comment.setNewsId(resultSet.getInt("news_id"));
            return comment;
        }
    }

    /**
     * Provides implementation of <code>PreparedStatementBuilder</code> for <code>Comment</code> entity
     *
     * @author aleh_rusak
     */
    private class CommentPreparedStatementBuilder implements PreparedStatementBuilder<Comment> {

        @Override
        public void buildPreparedStatement(Comment comment, PreparedStatement preparedStatement) throws SQLException {
            preparedStatement.setString(1, comment.getCommentText());
            preparedStatement.setTimestamp(2, new Timestamp(comment.getCreationDate().getTime()));
            preparedStatement.setInt(3, comment.getNewsId());
        }
    }
}
