package com.rfe.news.persistence.dao.template;

import com.rfe.news.exception.DAOException;
import com.rfe.news.persistence.AbstractConnectionManager;
import com.rfe.news.persistence.dao.util.DatabaseResourcesManager;
import com.rfe.news.persistence.dao.util.DatabaseUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Provides functionality for Transactional SQL-statements execution.
 * Uses given {@link AbstractConnectionManager} to obtain a {@link Connection}.
 *
 * Transaction can not be shared between several connections, so
 * to perform Transactional operations we have to provide the same {@link Connection}
 * object for all operations that are supposed to be executed in a Transaction.
 *
 * By default, every {@link Statement} or {@link PreparedStatement} execution
 * is automatically committed by implicit call of Connection's <code>commit();</code> method,
 * so we have to disable autocommit by calling <code>setAutoCommit(false);</code>
 *
 * @author aleh_rusak
 */
public final class TransactionExecutor implements QueryTemplate {

    private AbstractConnectionManager connectionManager;

    private Connection connection;

    public TransactionExecutor(AbstractConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    /**
     * Starts new Transaction: gets a connection from datasource and disables SQL-autocommit.
     */
    public void start() {
        try {
            connection = connectionManager.openConnection();
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            // Handle transaction opening exception.
        }
    }

    /**
     * Commits opened(!) Transaction and closes a Connection.
     * Beware of <code>TransactionExecutor</code> methods call order:
     * before committing a Transaction be sure that you have opened it!
     */
    public void commit() {
        try {
            connection.commit();
        } catch (SQLException e) {
            // Handle transaction committing exception
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    /**
     * Rollbacks opened(!) Transaction and closes a Connection.
     * Beware of <code>TransactionExecutor</code> methods call order:
     * before doing a rollback on a Transaction be sure that you have opened it!
     */
    public void rollback() {
        try {
            connection.rollback();
        } catch (SQLException e) {
            // Handle transaction rollback exception
        } finally {
            connectionManager.closeConnection(connection);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T, ID> ID create(T entity, String sqlQuery, String idColumn, PreparedStatementBuilder<T> statementMapper) throws DAOException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sqlQuery, new String[] {idColumn});
            statementMapper.buildPreparedStatement(entity, preparedStatement);
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                return (ID) resultSet.getObject(idColumn);
            } else {
                throw new DAOException("Entity was not saved to database.");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, resultSet);
        }
    }

    @Override
    public <T, ID> T update(T entity, ID entityId, String sqlQuery, PreparedStatementBuilder<T> statementMapper) throws DAOException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            statementMapper.buildPreparedStatement(entity, preparedStatement);
            int paramsCount = preparedStatement.getParameterMetaData().getParameterCount();
            preparedStatement.setObject(paramsCount, entityId);
            preparedStatement.executeUpdate();
            return entity;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, null);
        }
    }

    @Override
    public <T, ID> T read(ID entityId, String sqlQuery, RowMapper<T> rowMapper) throws DAOException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setObject(1, entityId);
            resultSet = preparedStatement.executeQuery();
            return resultSet.next() ? rowMapper.mapRow(resultSet) : null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, resultSet);
        }
    }

    @Override
    public <ID> void delete(ID entityId, final String sqlQuery) throws DAOException {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setObject(1, entityId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, null);
        }
    }

    @Override
    public boolean executeQuery(String sqlQuery) throws DAOException {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            return statement.execute(sqlQuery);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(statement, null);
        }
    }

    @Override
    public <T> T queryForObject(String sqlQuery, Object[] queryParameters, RowMapper<T> rowMapper) throws DAOException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            DatabaseUtils.fillInPreparedStatement(preparedStatement, queryParameters);
            resultSet = preparedStatement.executeQuery();
            return resultSet.next() ? rowMapper.mapRow(resultSet) : null;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, resultSet);
        }
    }

    @Override
    public <T> List<T> queryForList(final String sqlQuery, final Object[] queryParameters, RowMapper<T> rowMapper) throws DAOException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            DatabaseUtils.fillInPreparedStatement(preparedStatement, queryParameters);
            resultSet = preparedStatement.executeQuery();
            return DatabaseUtils.getResultsList(resultSet, rowMapper);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DatabaseResourcesManager.closeJdbcResources(preparedStatement, resultSet);
        }
    }

    @Override
    public <T> boolean exists(T entity) throws DAOException {
        throw new UnsupportedOperationException("Operation exists() is not supported.");
    }

    @Override
    public AbstractConnectionManager getConnectionManager() {
        throw new UnsupportedOperationException("TransactionExecutor doesn't provide Connection management.");
    }

    public Connection getConnection() {
        return connection;
    }
}
