package com.rfe.news.persistence.dao.template;

import com.rfe.news.exception.DAOException;
import com.rfe.news.persistence.AbstractConnectionManager;

import java.util.List;

/**
 * Declares behavior model template of performing database operations.
 * Supposed to be used in a DAO layer classes to simplify DAO implementation.
 *
 * @author aleh_rusak
 */
public interface QueryTemplate {

    /**
     * Provides implementation of Create (saving new object to database) template.
     *
     * @param entity entity to be inserted into database
     * @param sqlQuery SQL command
     * @param idColumn the name of auto generated PK column
     * @param statementMapper the {@link PreparedStatementBuilder}
     *                        to map persistent object on a create-statement
     * @return newly generated ID of inserted entity
     * @throws DAOException
     */
    <T, ID> ID create(T entity, final String sqlQuery, final String idColumn, PreparedStatementBuilder<T> statementMapper) throws DAOException;

    /**
     * Provides implementation of Read (retrieving object from database by it's ID) template.
     *
     * @param entityId the PK of instance
     * @param sqlQuery SQL command
     * @param rowMapper the {@link RowMapper} to map retrieved row on an entity
     * @return entity, retrieved from database
     * @throws DAOException
     */
    <T, ID> T read(ID entityId, final String sqlQuery, RowMapper<T> rowMapper) throws DAOException;

    /**
     * Provides implementation of Update (setting new values into database for specified entity) template
     *
     * @param entity object to be updated
     * @param sqlQuery SQL command
     * @param statementBuilder the {@link PreparedStatementBuilder}
     *                         to map persistent object on a update-statement
     * @return ID of updated entity
     * @throws DAOException
     */
    <T, ID> T update(T entity, ID entityId, final String sqlQuery, PreparedStatementBuilder<T> statementBuilder) throws DAOException;

    /**
     * Provides implementation of Delete (removing entity with given PK from database) template.
     *
     * @param entityId the id of entity that will be removed from database.
     * @param sqlQuery SQL command
     * @throws DAOException
     */
    <ID> void delete(ID entityId, final String sqlQuery) throws DAOException;

    /**
     * Executed given SQL command and attempts to build an Object from ResultSet
     *
     * @param sqlQuery SQL command
     * @param queryParameters parameters to be set on Statement
     * @param rowMapper the {@link RowMapper} to map retrieved row on an entity
     * @return Object, retrieved from database
     * @throws DAOException
     */
    <T> T queryForObject(final String sqlQuery, Object[] queryParameters, RowMapper<T> rowMapper) throws DAOException;

    /**
     * Executed given SQL command and attempts to build a List of Objects from ResultSet
     *
     * @param sqlQuery SQL command
     * @param queryParameters parameters to be set on Statement
     * @param rowMapper the {@link RowMapper} to map retrieved row on an entity
     * @return
     * @throws DAOException
     */
    <T> List<T> queryForList(final String sqlQuery, Object[] queryParameters, RowMapper<T> rowMapper) throws DAOException;

    /**
     * Checks, if given Entity already exists in a database.
     *
     * @param entity the Entity to check
     * @return <code>true</code>, if such Entity already exists,
     *         <code>false</code> otherwise.
     * @throws DAOException
     */
    <T> boolean exists(T entity) throws DAOException;

    /**
     * Executes given SQL command.
     *
     * @param sqlQuery SQL command
     * @return true, if SQL command returned any results
     * @throws DAOException
     */
    boolean executeQuery(final String sqlQuery) throws DAOException;

    /**
     * @return {@link AbstractConnectionManager} instance
     */
    AbstractConnectionManager getConnectionManager();
}
