package com.rfe.news.persistence.dao.util;

import com.rfe.news.util.PropertiesExtractor;

import java.util.Properties;

/**
 * Database connection configuration storage
 *
 * @author aleh_rusak
 */
public class DatabaseConfig {

    private static final DatabaseConfig config = new DatabaseConfig();

    private final String driverClassName;
    private final String url;
    private final String username;
    private final String password;

    public static DatabaseConfig getConfig() {
        return config;
    }

    private DatabaseConfig() {
        Properties properties = PropertiesExtractor.getPropertiesForPrefix("datasource.properties", "datasource.");
        driverClassName = properties.getProperty("datasource.driverClassName");
        url = properties.getProperty("datasource.url");
        username = properties.getProperty("datasource.username");
        password = properties.getProperty("datasource.password");
        try {
            // Load the Database Driver Class
            Class.forName(driverClassName);
        } catch (ClassNotFoundException e) {
            // Driver Class was not found
        }
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

}
