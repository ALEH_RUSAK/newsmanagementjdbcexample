package com.rfe.news.persistence.dao.util;

import com.rfe.news.exception.DAOException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Provides static functionality of database resource management
 *
 * @author aleh_rusak
 */
public class DatabaseResourcesManager {

    /**
     * {@link java.sql.PreparedStatement} is a sub-interface of {@link Statement}
     * so we don't have to provide two separate methods to close them.
     *
     * @param statement {@link Statement} object
     * @param resultSet {@link ResultSet} object
     * @throws DAOException
     */
    public static void closeJdbcResources(Statement statement, ResultSet resultSet) throws DAOException {
        try {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } finally {
                if (statement != null) {
                    statement.close();
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
