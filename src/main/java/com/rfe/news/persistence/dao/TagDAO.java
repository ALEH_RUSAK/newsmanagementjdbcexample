package com.rfe.news.persistence.dao;


import com.rfe.news.entity.Tag;
import com.rfe.news.exception.DAOException;

import java.util.Collection;
import java.util.List;

/**
 * {@link CrudOperations} extension for operations with Tag entity
 *
 * @author aleh_rusak
 */
public interface TagDAO extends CrudOperations<Tag, Integer> {

	/**
	 * Provides getting Tags for given News
	 *
     * @param newsId ID of news
	 * @return list of news tags
	 * @throws DAOException 
	 */
	List<Tag> findTagsForNews(Integer newsId) throws DAOException;
	
	/**
	 * Provides getting Tag by it's name
	 *
     * @param name tag's name
	 * @return tag by it's name
	 * @throws DAOException
	 */
	Tag findByName(String name) throws DAOException;

    /**
     * Provides binding several Tags to a News
     *
     * @param tagIds the list of Tags IDs
     * @param newsId ID of the News
     * @throws DAOException
     */
    void bindTagsToNews(Collection<Integer> tagIds, Integer newsId) throws DAOException;
}
