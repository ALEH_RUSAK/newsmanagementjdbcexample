package com.rfe.news.persistence.dao.util;

import java.util.Collection;

/**
 * Represents a SQL command.
 *
 * @author aleh_rusak
 */
public class Query {

    private String sqlTemplate;

    private Collection<Object> parameters;

    public Query() {
    }

    public String getSqlTemplate() {
        return sqlTemplate;
    }

    public void setSqlTemplate(String sqlTemplate) {
        this.sqlTemplate = sqlTemplate;
    }

    public Collection<Object> getParameters() {
        return parameters;
    }

    public void setParameters(Collection<Object> parameters) {
        this.parameters = parameters;
    }
}
