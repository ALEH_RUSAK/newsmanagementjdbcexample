package com.rfe.news.persistence;

import com.rfe.news.persistence.dao.util.DatabaseConfig;
import com.rfe.news.util.PropertiesExtractor;

import java.sql.Connection;
import java.util.Properties;

/**
 * Provides common abstraction for database Connection management.
 *
 * @author aleh_rusak
 */
public abstract class AbstractConnectionManager {

    protected DatabaseConfig config = DatabaseConfig.getConfig();

    public AbstractConnectionManager() {
    }

    /**
     * Creates a Connection to a database with specified Connection properties (url, username, password).
     *
     * @return established database Connection
     */
    public abstract Connection openConnection();

    /**
     * Drops given database Connection.
     *
     * @param connection Connection to drop (close)
     */
    public abstract void closeConnection(Connection connection);
}
