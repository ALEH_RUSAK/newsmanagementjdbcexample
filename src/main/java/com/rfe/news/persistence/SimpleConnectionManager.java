package com.rfe.news.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * The most simplified implementation of database Connection Manager.
 * Only provides functionality to open and close a Connection.
 *
 * This is a Singleton class.
 *
 * @author aleh_rusak
 */
public class SimpleConnectionManager extends AbstractConnectionManager {

    private static SimpleConnectionManager connectionManager;

    public static SimpleConnectionManager getConnectionManager() {
        if (connectionManager == null) {
            return connectionManager = new SimpleConnectionManager();
        }
        return connectionManager;
    }

    private SimpleConnectionManager() {
    }

    @Override
    public Connection openConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                    config.getUrl(),
                    config.getUsername(),
                    config.getPassword()
            );
        } catch (SQLException e) {
            // Handle SQLException if such has been thrown
        }
        return connection;
    }

    @Override
    public void closeConnection(Connection connection) {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            // Handle SQLException if such has been thrown
        }
    }
}
