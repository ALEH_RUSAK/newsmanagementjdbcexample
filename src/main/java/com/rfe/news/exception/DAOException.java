package com.rfe.news.exception;

public class DAOException extends Exception {

    private static final long serialVersionUID = 7878548672303746039L;

    public DAOException(Throwable e) {
        super(e);
    }

    public DAOException(String message) {
        super(message);
    }
}
