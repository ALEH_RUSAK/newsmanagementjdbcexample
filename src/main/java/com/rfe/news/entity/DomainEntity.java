package com.rfe.news.entity;

import java.io.Serializable;

/**
 * Abstract class represents a Current Domain Entity.
 *
 * @author aleh_rusak
 */
public abstract class DomainEntity<ID extends Serializable> implements Serializable {

    public abstract ID getId();
}
