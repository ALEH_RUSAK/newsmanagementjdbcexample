package com.rfe.news.entity;

import java.util.Date;

/**
 * Comment entity class. Mapped to "news_management.comment" table.
 * @author Aleh_Rusak
 */
public class Comment extends DomainEntity<Integer> {

    private static final long serialVersionUID = 1922295983020072188L;

    /**
     * Comment Id
     */
    private Integer id;

    /**
     * Comment's text
     */
    private String commentText;

    /**
     * Comment's creation date
     */
    private Date creationDate = new Date();

    /**
     * News for which comment was wrote
     */
    private Integer newsId;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comment comment = (Comment) o;

        if (id != null ? !id.equals(comment.id) : comment.id != null) return false;
        if (commentText != null ? !commentText.equals(comment.commentText) : comment.commentText != null) return false;
        if (creationDate != null ? !creationDate.equals(comment.creationDate) : comment.creationDate != null)
            return false;
        return !(newsId != null ? !newsId.equals(comment.newsId) : comment.newsId != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (commentText != null ? commentText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (newsId != null ? newsId.hashCode() : 0);
        return result;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }
}
