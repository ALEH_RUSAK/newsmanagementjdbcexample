package com.rfe.news.entity;

import com.rfe.news.persistence.annotation.Column;
import com.rfe.news.persistence.annotation.Identifier;
import com.rfe.news.persistence.annotation.Table;

/**
 * Tag entity class. Mapped to "news_management.tags" table.
 *
 * @author aleh_rusak
 */
@Table("news_management.tags")
public class Tag extends DomainEntity<Integer> {

    private static final long serialVersionUID = 3047185130962289441L;

    /**
     * Tag ID
     */
    @Identifier
    @Column("tag_id")
    private Integer tagId;

    /**
     * Tag name
     */
    @Column("tag_name")
    private String tagName;

    @Override
    public Integer getId() {
        return tagId;
    }

    public void setId(Integer tagId) {
        this.tagId = tagId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tag tag = (Tag) o;

        if (tagId != null ? !tagId.equals(tag.tagId) : tag.tagId != null) return false;
        return !(tagName != null ? !tagName.equals(tag.tagName) : tag.tagName != null);

    }

    @Override
    public int hashCode() {
        int result = tagId != null ? tagId.hashCode() : 0;
        result = 31 * result + (tagName != null ? tagName.hashCode() : 0);
        return result;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
