package com.rfe.news.entity;

/**
 * Author entity class. Mapped to "news_management.authors" table.
 *
 * @author aleh_rusak
 */
public class Author extends DomainEntity<Integer> {

    private static final long serialVersionUID = 4494387171796587039L;

    /**
     * Author ID
     */
    private Integer id;

    /**
     * Name of author
     */
    private String authorName;

    /**
     * Author's email
     */
    private String authorEmail;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (id != null ? !id.equals(author.id) : author.id != null) return false;
        if (authorName != null ? !authorName.equals(author.authorName) : author.authorName != null) return false;
        return !(authorEmail != null ? !authorEmail.equals(author.authorEmail) : author.authorEmail != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 31 * result + (authorEmail != null ? authorEmail.hashCode() : 0);
        return result;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }
}
