package com.rfe.news.entity;

import java.util.Date;

/**
 * News entity. Declared as Value Object, e.g. instances of that class are fully instantiated
 * "news" objects with Author.
 * Also has mapping on a "news_management.news" table
 *
 * @author aleh_rusak
 */
public class News extends DomainEntity<Integer> {

    private static final long serialVersionUID = -5903962588987544260L;

    /**
     * News ID
     */
    private Integer id;

    /**
     * News title
     */
    private String title;

    /**
     * News brief
     */
    private String brief;

    /**
     * News content
     */
    private String content;

    /**
     * News date of publishing
     */
    private Date creationDate = new Date();

    /**
     * News Author
     */
    private Author author;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        News news = (News) o;

        if (id != null ? !id.equals(news.id) : news.id != null) return false;
        if (title != null ? !title.equals(news.title) : news.title != null) return false;
        if (brief != null ? !brief.equals(news.brief) : news.brief != null) return false;
        if (content != null ? !content.equals(news.content) : news.content != null) return false;
        if (creationDate != null ? !creationDate.equals(news.creationDate) : news.creationDate != null) return false;
        return !(author != null ? !author.equals(news.author) : news.author != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (brief != null ? brief.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
