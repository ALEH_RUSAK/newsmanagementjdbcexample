package com.rfe.news.util;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Provides static functionality of extracting property-values from files.
 *
 * @author aleh_rusak
 */
public class PropertiesExtractor {

    public static Properties getProperties(String resourcePath) {
        Properties properties = null;
        try {
            InputStream sourceStream = PropertiesExtractor.class.getClassLoader().getResourceAsStream(resourcePath);
            if (sourceStream != null) {
                properties = new Properties();
                properties.load(sourceStream);
            } else {
                throw new RuntimeException("There is no properties at specified path.");
            }
        } catch (IOException e) {
            // Handle some IOException
        }
        return properties;
    }

    // Here Stream API is used.
    // Stream API and Lambda expressions are supported only by Java 8.
    // Great article about Stream API: https://habrahabr.ru/company/luxoft/blog/270383/
    public static Properties getPropertiesForPrefix(String resourcePath, String prefix) {
        Properties sourceProperties = getProperties(resourcePath);
        Properties result = new Properties();
        sourceProperties.keySet().stream()
                .filter(key -> key != null)
                .map(Object::toString)
                .filter(key -> key.startsWith(prefix))
                .forEach(key -> result.setProperty(key, sourceProperties.getProperty(key)));
        return result;
    }

    private PropertiesExtractor() {
    }
}
